# Esp32 Without Development Board

Set up a bare esp32 without development board.

## Parts

* [ESP32-Wroom-32](https://aliexpress.com/item/1005003057630388.html)
* [Mini DC-DC 12-24V to 1.8-12V 3A step down converter](https://aliexpress.com/item/4000938525410.html):
* SMD resistors 10kΩ
* SMD capacitors 10µF, 0.1µF
* Jumper or push button switch

## Assembly

* Connect the DC-DC converter to a 5V power supply (ground is the same for input and output).
* Adjust the output of the converter to 3.3V (I had do connect the resistor labeled 5V and then adjust to 3.3V using the mini potentionmeter on the DC-DC converter)
* Solder the 10µF (further away from esp32) and 0.1µF (closer to the esp32) capacitors between the GND and 3.3V wires.
* Connect to `GND` and `3.3V` of the ESP32.
* Solder a 10kΩ resistor to the 3.3V line and connect to the `EN` pin of the ESP32
* Install a jumper or push button switch and a 10kΩ resistor between GND and `IO0` of the ESP32. Close the jumper/switch when you want to flash the ESP32.
* Connect the appropriate GPIO pins including 10kΩ pull-up or pull-down resistors.

